package com.cimforce.eurekaserver;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

@EnableEurekaServer
@SpringBootApplication
@Slf4j
public class EurekaServerApplication {

    public static void main(String[] args) throws UnknownHostException {
        long startTime = System.currentTimeMillis();
        log.info("=========================== 啟動 Eureka Server Service ===========================");
        log.info("args:{}", JSONUtil.toJsonStr(args));
        SpringApplication application = new SpringApplication(EurekaServerApplication.class);
        Properties properties = new Properties();
        // 關閉無用打印
        properties.setProperty("logging.pattern.console", "");
        application.setDefaultProperties(properties);

        // 取得環境相關資訊
        ConfigurableApplicationContext configurableApplicationContext = application.run(args);
        Environment env = configurableApplicationContext.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        port = port == null ? "8080" : port;
        String eurekaPath = env.getProperty("tankfinal.cloud.eureka.ip");
        String applicationName = env.getProperty("spring.application.name");
        log.info("\n================= 啟動完成 耗时: {} ms ================="
                + "\n\t服務名稱: {}\n"
                + "\t外部訪問地址: http://{}:{}/\n"
                + "\tEureka訪問位置: http://{}:8761/\n"
                + "=========================================================", (System.currentTimeMillis() - startTime), applicationName, ip, port, eurekaPath);
    }
}
